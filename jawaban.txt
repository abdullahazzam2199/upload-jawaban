(1).MariaDB [(none)]> create database myshop1;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]>

(2).create table bisnis1(
    -> id int(8) primary key auto_increment,
    -> nama varchar(30)
    -> );

create table usaha(
    -> id int(8) primary key auto_increment,
    ->  judul varchar(60),
    -> harga int(20),
    -> bisnis1_id int(8),
    -> foreign key(bisnis1_id) references bisnis1(id)
    -> );

(3.)
A.table bisnis 1
insert into bisnis1(nama) values("kuliner"),("konveksi"),("transportasi");
Query OK, 3 rows affected (0.002 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop1]> select * from bisnis1;
+----+--------------+
| id | nama         |
+----+--------------+
|  1 | kuliner      |
|  2 | konveksi     |
|  3 | transportasi |
+----+--------------+
3 rows in set (0.000 sec)


B.table usaha
insert into usaha(judul,harga,bisnis1_id) values("prospektif",20000,1),("beresiko",1500,3),("stagnan",17500,1);
Query OK, 3 rows affected (0.002 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop1]> select * from usaha;
+----+------------+-------+------------+
| id | judul      | harga | bisnis1_id |
+----+------------+-------+------------+
|  1 | prospektif | 20000 |          1 |
|  2 | beresiko   |  1500 |          3 |
|  3 | stagnan    | 17500 |          1 |
+----+------------+-------+------------+
3 rows in set (0.000 sec)

(4.)
A.select judul,harga,id from usaha;

B.MariaDB [myshop1]>  select * from usaha where harga>15000;
+----+------------+-------+------------+
| id | judul      | harga | bisnis1_id |
+----+------------+-------+------------+
|  1 | prospektif | 20000 |          1 |
|  3 | stagnan    | 17500 |          1 |
+----+------------+-------+------------+
2 rows in set (0.000 sec)

C.MariaDB [myshop1]> select * from usaha where judul like '%res%';
+----+----------+-------+------------+
| id | judul    | harga | bisnis1_id |
+----+----------+-------+------------+
|  2 | beresiko |  1500 |          3 |
+----+----------+-------+------------+
1 row in set (0.000 sec)

D.select usaha.id,usaha.judul, usaha.harga,usaha.bisnis1_id,bisnis1.nama from usaha inner join bisnis1 on usaha.bisnis1_id = bisnis1.id;
+----+------------+-------+------------+--------------+
| id | judul      | harga | bisnis1_id | nama         |
+----+------------+-------+------------+--------------+
|  1 | prospektif | 20000 |          1 | kuliner      |
|  3 | stagnan    | 17500 |          1 | kuliner      |
|  2 | beresiko   |  1500 |          3 | transportasi |
+----+------------+-------+------------+--------------+
3 rows in set (0.000 sec)

(5.)MariaDB [myshop1]> select * from usaha;
+----+------------+-------+------------+
| id | judul      | harga | bisnis1_id |
+----+------------+-------+------------+
|  1 | prospektif | 20000 |          1 |
|  2 | beresiko   |  1500 |          3 |
|  3 | stagnan    | 17500 |          1 |
+----+------------+-------+------------+
3 rows in set (0.000 sec)

MariaDB [myshop1]> update usaha set harga=18000 where id=2;
Query OK, 1 row affected (0.002 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop1]> select * from usaha;
+----+------------+-------+------------+
| id | judul      | harga | bisnis1_id |
+----+------------+-------+------------+
|  1 | prospektif | 20000 |          1 |
|  2 | beresiko   | 18000 |          3 |
|  3 | stagnan    | 17500 |          1 |
+----+------------+-------+------------+
3 rows in set (0.000 sec)

MariaDB [myshop1]>
